Ext.define('Pertemuan.store.DetailPersonnel', {
    extend: 'Ext.data.Store',
    storeId: 'detailpersonnel',
    alias: 'store.detailpersonnel',
    autoLoad: true,
    autoSync: true,
    fields: [
        'user_id','npm','name', 'email', 'phone', 'image'
    ],

    proxy: {
        type: 'jsonp',
        api :{
            read: "http://localhost/MyApp_php/readPersonnel.php",
            update: "http://localhost/MyApp_php/updatePersonnel.php"
        },
        reader: {
            type: 'json',
            rootProperty: 'items'
        }
    },

    listeners: {
        beforesync: function(options, e0pts){
            if(typeof options.update != "undefine data"){
                Ext.toast('The assigment is success');
            };
        }
    }
});
