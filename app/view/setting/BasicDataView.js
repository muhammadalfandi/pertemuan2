Ext.define('Pertemuan.view.setting.BasicDataView', {
    extend: 'Ext.Container',
    xtype: 'basicdataview',
    requires: [
        'Ext.dataview.plugin.ItemTip',
        'Ext.plugin.Responsive',
        'Pertemuan.store.Personnel',
        'Ext.field.Search'
    ],
    
    layout: 'fit',
    cls: 'ks-basic demo-solid-background',
    shadow: true,
    // viewModel: {
    //     stores : {
    //         detailpersonnel: {
    //             type : 'detailpersonnel'
    //         }
    //     }
    // },
    items: [        
        {
        docked: 'top',
        xtype: 'toolbar',
        items: [
            {
                xtype: 'spacer'
            },
            {
                xtype: 'searchfield',
                placeHolder: 'Search by NPM',
                name: 'searchfield',
                listeners: {
                    change: function( me, newValue, oldValue, e0pts){
                        detailStore = Ext.getStore('personnel');
                        detailStore.filter('npm', newValue);
                    }
                }
            },
            {
                xtype: 'spacer'
            },
            {
                xtype: 'searchfield',
                placeHolder: 'Search by Name',
                name: 'searchfield',
                listeners: {
                    change: function( me, newValue, oldValue, e0pts){
                        detailStore = Ext.getStore('personnel');
                        detailStore.filter('name', newValue);
                    }
                }
            },
            {
                xtype: 'spacer'
            },
            {
                xtype: 'searchfield',
                placeHolder: 'Search by Email',
                name: 'searchfield',
                listeners: {
                    change: function( me, newValue, oldValue, e0pts){
                        detailStore = Ext.getStore('personnel');
                        detailStore.filter('email', newValue);
                    }
                }
            },
            {
                xtype: 'spacer'
            }
            ]
        },
        {
            xtype: 'dataview',
            scrollable: 'y',
            cls: 'dataview-basic',
            itemTpl: '<table style="border-spacing:5px; border-collapse:separate">'+
            '<tr><td rowspan= 5><img class="image" src="resources/{image}" width=100></td></tr>' +
            '<tr><td><font size = 4><b>{name}</b></font><br><font color = blue><b>{npm}</b></font></td></tr>' +
            '<tr><td><font size = 4><b>{email}</b></font><br><font size = 3><u>{phone}</u></font></td></tr>' +
            '<hr>',
            store: 'personnel',
            // bind: {
            //     store: '{detailpersonnel}'
            // },
            plugins: {
            type: 'dataviewtip',
            align: 'l-r?',
            plugins: 'responsive',
            
            // On small form factor, display below.
            responsiveConfig: {
                "width < 600": {
                    align: 'tl-bl?'
                }
            },
            width: 600,
            minWidth: 300,
            delegate: '.image',
            allowOver: true,
            anchor: true,
            bind: '{record}',
            tpl: '<table style="border-spacing:3px;border-collapse:separate">' +
                    '<tr><td><p><b> Information of User </b></p></td></tr>' +
                    '<tr><td>NPM: </td><td>{npm}</td></tr>' +
                    '<tr><td>Name:</td><td>{name}</td></tr>' + 
                    '<tr><td>Email:</td><td>{email}</td></tr>' + 
                    '<tr><td>Phone:</td><td>{phone}</td></tr>' 
        }
    }]
});