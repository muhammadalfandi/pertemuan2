Ext.define('Pertemuan.view.setting.DetailView', {
    extend: 'Ext.Container',
    xtype: 'mydetail',
    requires: [
        'Ext.dataview.plugin.ItemTip',
        'Ext.plugin.Responsive',
        //'Pertemuan.store.DetailPersonnel'
    ],

    
    layout: 'fit',
    cls: 'ks-basic demo-solid-background',
    shadow: true,

    viewModel :{
        stores:{
            personnel:{
                type: 'personnel'
            }
        }
    },

    items: [{
        xtype: 'dataview',
        scrollable: 'y',
        cls: 'dataview-basic',
        id: 'mydataview',
        itemTpl: '<table style="border-spacing:5px; border-collapse:separate">'+
        '<tr><td rowspan= 5><img class="image" src="resources/{image}" width=100></td></tr>' +
        '<tr><td><font size = 4><b>{name}</b></font><br><font color = blue><b>{npm}</b></font></td></tr>' +
        '<tr><td><font size = 4><b>{email}</b></font><br><font size = 3><u>{phone}</u></font></td></tr>' +
        '<tr><td><button type=button onclick="onDeletePersonnel({user_id})">Hapus</button> <button type=button onclick="onUpdatePersonnel({user_id})">Update</button></td></tr>' +
        '<hr>',
        bind:{
            store: '{personnel}'
        },
    }]
});