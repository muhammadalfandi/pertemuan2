Ext.define('Pertemuan.view.chart.DataChart', {
    extend: 'Ext.Container',
    xtype: 'datachart',
    requires: [
        'Ext.carousel.Carousel',
        'Pertemuan.view.chart.Column',
        'Pertemuan.view.chart.Stacked',
        'Pertemuan.view.chart.GaugeChart'
    ],

    cls: 'cards',
    shadow: true,
    layout: {
        type: 'vbox',
        align: 'stretch'
    },
    defaults: {
        cls: 'demo-solid-background',
        flex: 1
    },
    items: [{
        xtype: 'carousel',
        items: [
        {
            layout: 'fit',
            items: [{
                xtype: 'panel',
                layout: 'vbox',
                items: [{
                    xtype: 'mystacked',
                    flex: 1
                },{
                    xtype: 'listbar',
                    flex: 1
                }]
            }]
        },
        {
            layout: 'fit',
            items: [{
                xtype: 'column-chart'
            }]
        },
        {
            layout: 'fit',
            items: [{
                xtype: 'mygauge'
            }]
        }]
    }]
});