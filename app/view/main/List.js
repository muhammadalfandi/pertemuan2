/**
 * This view is an example list of people.
 */
Ext.define('Pertemuan.view.main.List', {
    extend: 'Ext.grid.Grid',
    xtype: 'mainlist',

    requires: [
        'Ext.grid.plugin.Editable',
    ],

    plugins: [{
        type: 'grideditable'
    }],

    title: 'Daftar Anggota',

    //store: {
        //type: 'personnel'
    //},
    bind: '{personnel}',

    viewModel: {
        stores:{
            personnel:{
                type: 'personnel'
            }
        }
    },

    columns: [
        { text: 'NPM',  dataIndex: 'npm', flex: 1},
        { text: 'Nama',  dataIndex: 'name', flex: 1, editable: true },
        { text: 'Email', dataIndex: 'email', flex: 1, editable: true },
        { text: 'Telepon', dataIndex: 'phone', flex: 1, editable: true }
    ],

    listeners: {
        select: 'onItemSelected'
    }
});
