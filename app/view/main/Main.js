/**
 * This class is the main view for the application. It is specified in app.js as the
 * "mainView" property. That setting causes an instance of this class to be created and
 * added to the Viewport container.
 *
 * TODO - Replace the content of this view to suit the needs of your application.
 */
Ext.define('Pertemuan.view.main.Main', {
    extend: 'Ext.tab.Panel',
    xtype: 'app-main',
    id: 'app-main',

    requires: [
        'Ext.MessageBox',

        'Pertemuan.view.main.MainController',
        'Pertemuan.view.main.MainModel',
        'Pertemuan.view.main.List',
        'Pertemuan.view.form.User',
        'Pertemuan.view.group.Carousel',
        'Pertemuan.view.setting.BasicDataView',
        'Pertemuan.view.form.Login',
        'Pertemuan.view.chart.DataChart',
        'Pertemuan.view.tree.TreeList',
        'Pertemuan.view.tree.TreePanel',
        'Pertemuan.view.form.UserEdit',
        'Pertemuan.view.setting.DetailView',
        'Pertemuan.view.main.ListBar'
    ],

    controller: 'main',
    viewModel: 'main',

    defaults: {
        tab: {
            iconAlign: 'top'
        },
        styleHtmlContent: true
    },

    tabBarPosition: 'bottom',

    items: [
        {
            docked: 'top',
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Refresh',
                    ui: 'action',
                    scope: this,
                    listeners: {
                        tap: 'onRefresh'
                }
            }]
        },
        {
            title: 'Home',
            iconCls: 'x-fa fa-home',
            layout: 'fit',
            // The following grid shares a store with the classic version's grid as well!
            items: [{
                xtype: 'panel',
                layout: 'hbox',
                items: [{
                    xtype: 'mainlist',
                    flex: 2
                },{
                    xtype: 'mydetail',
                    flex: 1
                },{
                    xtype: 'useredit',
                    flex: 1
                }]
                
            }]
        },{
            title: 'Users',
            iconCls: 'x-fa fa-user',
            layout: 'fit',
            items: [{
                xtype: 'user'
            }]
        },{
            title: 'Groups',
            iconCls: 'x-fa fa-users',
            layout: 'fit',
            items: [{
                xtype: 'Carousel'
            }]
        },
        {
        title: 'Settings',
        iconCls: 'x-fa fa-cog',
        layout: 'fit',
        items: [{
            xtype: 'basicdataview'
        }]
        },
        {
            title: 'Chart',
            iconCls: 'x-fa fa-bar-chart',
            layout: 'fit',
            items: [{
                xtype: 'datachart'
            }]
        },{
            title: 'Tree',
            iconCls: 'x-fa fa-sitemap',
            layout: 'fit',
            items: [{
                xtype: 'tree-panel'
            }]
        }
    ]
});
