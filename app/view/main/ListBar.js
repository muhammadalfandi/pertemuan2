Ext.define('Pertemuan.view.main.ListBar', {
    extend: 'Ext.grid.Grid',
    xtype: 'listbar',

    requires: [
        'Ext.grid.plugin.Editable',
        'Pertemuan.store.StackedStore'
    ],

    plugins: [{
        type: 'grideditable'
    }],

    title: 'List Bar',

    //store: {
        //type: 'personnel'
    //},

    bind: '{bar}',

    viewModel: {
        stores:{
            bar:{
                type: 'bar'
            }
        }
    },

    columns: [
        { text: 'Name',  dataIndex: 'name', flex: 1, editable: true },
        { text: 'g1',  dataIndex: 'g1', flex: 1, editable: true },
        { text: 'g2',  dataIndex: 'g2', flex: 1, editable: true },
        { text: 'g3',  dataIndex: 'g3', flex: 1, editable: true },
        { text: 'g4',  dataIndex: 'g4', flex: 1, editable: true },
        { text: 'g5',  dataIndex: 'g5', flex: 1, editable: true },
        { text: 'g6',  dataIndex: 'g6', flex: 1, editable: true },
    ],

    listeners: {
        select: 'onBarItemSelected'
    }
});
