Ext.define('Pertemuan.view.form.LoginController',{
    extend: 'Ext.app.ViewController',
    alias: 'controller.login',

    onLogin: function(){
        var form = this.getView();
        var username = form.getFields('username').getValue();
        var password = form.getFields('password').getValue();

        if(username && password){
            if(username=="admin" && password=="kolonel123"){
                localStorage.setItem('username', username);
                localStorage.setItem('password', password);
                localStorage.setItem('loggedIn', true);
                form.hide();
                Ext.Msg.alert('<center><b>Selamat Datang Admin!</b></center>');
            }
            else{
                Ext.Msg.alert('<center><b>Anda Bukan Admin!</b></center>');
            }
        }
        else{
            Ext.Msg.alert('<center><b>Anda Bukan Admin!</b></center>');
        }
    }
    
});