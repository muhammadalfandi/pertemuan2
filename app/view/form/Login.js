Ext.define('Pertemuan.view.form.Login', {
    extend: 'Ext.form.Panel',

    requires: [
        'Ext.form.FieldSet',
        'Ext.field.Password',
    ],
    shadow: true,
    cls: 'demo-solid-background',
    controller: 'login',
    xtype: 'login',
    id: 'login',
    items: [
        {
            xtype: 'fieldset',
            id: 'fieldsetLogin',
            title: '<br><center><font size=6><b>Login Form</b></font></center><br>',
            instructions: '<font size=4><b>Only Administrator!</b></font>',
            defaults: {
                labelWidth: '35%'
            },
            items: [
                {
                    xtype: 'textfield',
                    name: 'username',
                    label: '<font size=3><b>Username</b></font>',
                    placeHolder: 'Input Username',
                    autoCapitalize: true,
                    required: true,
                    clearIcon: true
                },
                {
                    xtype: 'passwordfield',
                    revealable: true,
                    name : 'password',
                    label: '<font size=3><b>Password</b></font>',
                    placeHolder: 'Input Password',
                    clearIcon: true
                }
            ]
        },
        {
            xtype: 'container',
            defaults: {
                xtype: 'button',
                style: 'margin: 1em',
                flex: 1
            },
            layout: {
                type: 'hbox'
            },
            items: [
                {
                    text: 'Login',
                    ui: 'action',
                    //scope: this,
                    hasDisabled: false,
                    handler: 'onLogin'
                },
            ]
        }
    ]
});