Ext.define('Pertemuan.view.form.UserEdit', {
    extend: 'Ext.form.Panel',

    shadow: true,
    cls: 'demo-solid-background',
    xtype: 'useredit',
    id: 'useredit',
    items: [
        {
            xtype: 'textfield',
            name: 'npm',
            label: 'NPM',
            id: 'mynpm',
            placeHolder: 'Your NPM',
            autoCapitalize: true,
            required: true,
            clearIcon: true
        },
        {
            xtype: 'textfield',
            name: 'name',
            label: 'Name',
            id: 'myname',
            placeHolder: 'Your Name',
            autoCapitalize: true,
            required: true,
            clearIcon: true
        },
        {
            xtype: 'emailfield',
            name: 'email',
            label: 'Email',
            id: 'myemail',
            placeHolder: 'me@sencha.com',
            clearIcon: true
        },
        {
            xtype: 'textfield',
            name: 'phone',
            label: 'Phone',
            id: 'myphone',
            placeHolder: '08xxx',
            clearIcon: true
        },
        {
            xtype: 'textfield',
            name: 'image',
            label: 'Image',
            id: 'myimage',
            placeHolder: 'user1.png or user2.png',
            autoCapitalize: true,
            required: true,
            clearIcon: true
        },
        {
            html :  '<table style="border-spacing:5px; border-collapse:separate">'+
                    '<tr><td><font size = 3>Please input image based name image on below : </font></td></tr>' +
                    '<tr><td align = "center"><img src="resources/user1.png" width=70></td></tr>' +
                    '<tr><td align = "center"><font size = 3><b>user1.png</b></font></td></tr> ' +
                    '<tr><td><hr></td></tr>' +
                    '<tr><td align = "center"><img src="resources/user2.png" width=70></td></tr>' +
                    '<tr><td align = "center"><font size = 3><b>user2.png</b></font></td></tr>',
        },
        {
            xtype : 'spacer'
        },
        {
            xtype: 'button',
            text: 'Update Data',
            ui: 'action',
            handler: 'onEditUser'
        },    
        {
            xtype: 'button',
            text: 'Add Data',
            ui: 'confirm',
            handler: 'onAddUser'
        },  
    ]
});