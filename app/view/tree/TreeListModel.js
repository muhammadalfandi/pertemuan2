// Ext.define('Pertemuan.view.tree.TreeListModel', {
//     extend: 'Ext.app.ViewModel',
    
//     alias: 'viewmodel.tree-list',

//     formulas: {
//         selectionText: function(get) {
//             var selection = get('treelist.selection'),
//                 path;
//             if (selection) {
//                 path = selection.getPath('text');
//                 path = path.replace(/^\/Root/, '');
//                 return 'Selected: ' + path;
//             } else {
//                 return 'No node selected';
//             }
//         }
//     }, 

//     stores: {
//         navItems: {
//             type: 'tree',
//             rootVisible: true,
//             root: {
//                 expanded: true,
//                 text: 'All',
//                 html: '<table style="border-spacing:5px; border-collapse:separate">'+
//                       '<tr><td rowspan= 5><img src="../../resources/test.png" width=100></td></tr>' +
//                       '<tr><td><font size = 4><b><p>Education</p></b></font></font></td></tr>' +
//                       '<tr><td><font size = 2><b><p>Learning about sencha tree component</p></b></font></font></td></tr>',
//                 iconCls: 'x-fa fa-sitemap',
//                 children: [{
//                     text: 'Home',
//                     html: '<table style="border-spacing:5px; border-collapse:separate">'+
//                     '<tr><td rowspan= 5><img src="../../resources/home.png" width=200></td></tr>' +
//                     '<tr><td><font size = 4><b><p>Education</p></b></font></font></td></tr>' +
//                     '<tr><td><font size = 4><b><p>This Is Page of Home</p></b></font></font></td></tr>',
//                     iconCls: 'x-fa fa-home',
//                     children: [{
//                         text: 'Messages',
//                         html: '<table style="border-spacing:5px; border-collapse:separate">'+
//                         '<tr><td rowspan= 5><img src="../../resources/user1.png" width=100></td></tr>' +
//                         '<tr><td><font size = 4><b><p>Education</p></b></font></font></td></tr>' +
//                         '<tr><td><font size = 4><b><p>This Is Page of Messages</p></b></font></font></td></tr>',
//                         iconCls: 'x-fa fa-inbox',
//                         leaf: true
//                     }]
//                 }, {
//                     text: 'Users',
//                     html: '<table style="border-spacing:5px; border-collapse:separate">'+
//                     '<tr><td rowspan= 5><img src="../../resources/foto2.jpg" width=100></td></tr>' +
//                     '<tr><td><font size = 4><b><p>Name : Muhammad Alfandi</p></b></font></font></td></tr>' +
//                     '<tr><td><font size = 4><b><p>NPM  : 183510165</p></b></font></font></td></tr>'+

//                     '<hr>' + '<table style="border-spacing:5px; border-collapse:separate">'+
//                     '<tr><td rowspan= 5><img src="../../resources/user2.png" width=100></td></tr>' +
//                     '<tr><td><font size = 4><b><p>Name : Dwi Amrisa</p></b></font></font></td></tr>' +
//                     '<tr><td><font size = 4><b><p>NPM  : 183510257</p></b></font></font></td></tr>' +

//                     '<hr>' + '<table style="border-spacing:5px; border-collapse:separate">'+
//                     '<tr><td rowspan= 5><img src="../../resources/user1.png" width=100></td></tr>' +
//                     '<tr><td><font size = 4><b><p>Name : Maulana</p></b></font></font></td></tr>' +
//                     '<tr><td><font size = 4><b><p>Npm  : 183510199</p></b></font></font></td></tr>' +
                    
//                     '<hr>' + '<table style="border-spacing:5px; border-collapse:separate">'+
//                     '<tr><td rowspan= 5><img src="../../resources/user1.png" width=100></td></tr>' +
//                     '<tr><td><font size = 4><b><p>Name : Dana</p></b></font></font></td></tr>' +
//                     '<tr><td><font size = 4><b><p>Npm  : 183510211</p></b></font></font></td></tr>' +
//                     '<hr>',
//                     iconCls: 'x-fa fa-users',
//                     children: [{
//                         text: 'User 1',
//                         html: '<table style="border-spacing:5px; border-collapse:separate">'+
//                         '<tr><td rowspan= 5><img src="../../resources/foto2.jpg" width=100></td></tr>' +
//                         '<tr><td><font size = 4><b><p>Name : Muhammad Alfandi</p></b></font></font></td></tr>' +
//                         '<tr><td><font size = 4><b><p>NPM  : 183510165</p></b></font></font></td></tr>',
//                         iconCls: 'x-fa fa-user',
//                         leaf: true
//                     }, {
//                         text: 'User 2',
//                         html: '<table style="border-spacing:5px; border-collapse:separate">'+
//                         '<tr><td rowspan= 5><img src="../../resources/user2.png" width=100></td></tr>' +
//                         '<tr><td><font size = 4><b><p>Name : Dwi Amrisa</p></b></font></font></td></tr>' +
//                         '<tr><td><font size = 4><b><p>NPM  : 183510257</p></b></font></font></td></tr>',
//                         iconCls: 'x-fa fa-user',
//                         leaf: true
//                     }, {
//                         text: 'User 3',
//                         html: '<table style="border-spacing:5px; border-collapse:separate">'+
//                         '<tr><td rowspan= 5><img src="../../resources/user1.png" width=100></td></tr>' +
//                         '<tr><td><font size = 4><b><p>Name : Maulana</p></b></font></font></td></tr>' +
//                         '<tr><td><font size = 4><b><p>Npm  : 183510199</p></b></font></font></td></tr>',
//                         iconCls: 'x-fa fa-user',
//                         leaf: true
//                     }, {
//                         text: 'User 4',
//                         html: '<table style="border-spacing:5px; border-collapse:separate">'+
//                         '<tr><td rowspan= 5><img src="../../resources/user1.png" width=100></td></tr>' +
//                         '<tr><td><font size = 4><b><p>Name : Dana</p></b></font></font></td></tr>' +
//                         '<tr><td><font size = 4><b><p>Npm  : 183510211</p></b></font></font></td></tr>',
//                         iconCls: 'x-fa fa-user',
//                         leaf: true
//                     }]
//                 }]
//             }
//         }
//     }
// });

Ext.define('Pertemuan.view.tree.TreeListModel', {
    extend: 'Ext.app.ViewModel',

    alias: 'viewmodel.tree-list',

    formulas: {
        selectionText: function(get) {
            var selection = get('treelist.selection'),
                path;
            if (selection) {
                path = selection.getPath('text');
                path = path.replace(/^\/Root/, '');
                return 'Selected: ' + path;
            } else {
                return 'No node selected';
            }
        }
    }, 

    stores: {
        navItems: {
            type: 'tree',
            rootVisible: true,
            root: {
                expanded: true,
                text: 'All',
                iconCls: 'x-fa fa-sitemap',
                children: [{
                    text: 'Home',
                    iconCls: 'x-fa fa-home',
                    children: [{
                        text: 'Messages',
                        iconCls: 'x-fa fa-inbox',
                        leaf: true
                    }]
                }, {
                    text: 'Users',
                    iconCls: 'x-fa fa-users',
                    children: [{
                        text: 'User 1',
                        iconCls: 'x-fa fa-user',
                        leaf: true
                    }, {
                        text: 'User 2',
                        iconCls: 'x-fa fa-user',
                        leaf: true
                    }]
                }]
            }
        }
    }
});